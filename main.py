#! /usr/bin/python

# Trabajo practico laboratorio
# Complementos Matematicos I
# Aseguinolaza Luis
# Nielsen Maximiliano

import argparse
import matplotlib.pyplot as plt
import numpy as np
import utils
import random

from matplotlib.patches import Ellipse
from matplotlib import collections  as mc

class LayoutGraph:

    def __init__(self, grafo, iters, refresh, ancho, alto, c1, c2, t0, ct, g, eps, verbose = False, nodeNames = False, fontsize = 200, axes = False):
        """
        Parámetros:
        grafo:      grafo en formato lista
        iters:      cantidad de iteraciones a realizar
        refresh:    cada cuántas iteraciones graficar. Si su valor es cero, entonces debe graficarse solo al final.
        ancho:      ancho del canvas
        alto:       alto del canvas
        c1:         constante de repulsión
        c2:         constante de atracción
        t0:         temperatura inicial
        ct:         constante de disminucion de temperatura
        g:          constante de gravedad
        eps:        distancia minima entre nodos
        verbose:    si está encendido, activa los comentarios
        nodeNames:  si esta encencido, plotea los nombres de los nodos
        fontsize:   tamaño del nombre de los nodos
        axes:       si esta encendido, plotea los ejes
        """

        # Guardo el grafo
        self.grafo = grafo
        self.ancho = ancho
        self.alto = alto
        self.area = ancho * alto
        self.c1 = c1
        self.c2 = c2
        self.t = t0
        self.ct = ct
        self.g = g
        self.eps = eps

        # Inicializo estado
        # Completar
        self.posiciones = {}
        self.fuerzas = {}
        self.posiciones = utils.randomize_position(self.grafo[0], self.ancho, self.alto)

        # Guardo opciones
        self.iters = iters
        self.verbose = verbose
        self.refresh = refresh
        self.nodeNames = nodeNames
        self.fontsize = fontsize
        self.axes = axes

    def printPos(self):
        """
        printPos :: () -> ()
        Imprime las posiciones de todos los nodos por consola.
        """
        for nodo in self.grafo[0]:
            print("Nodo "+nodo+" en pos: "+str(self.posiciones[nodo][0])+" "+str(self.posiciones[nodo][1]))
   
    def printFuer(self):
        """
        printFuer :: () -> ()
        Imprime las fuerzas ejercidas sobre cada nodo por consola.
        """
        for nodo in self.grafo[0]:
            print("Nodo "+nodo+" fuerza: ("+str(self.fuerzas[nodo][0])+" "+str(self.fuerzas[nodo][1])+")")
    
    def printDist(self):
        """
        printDist :: () -> ()
        Imprime la distancia entre cada par de nodos por consola.
        """
        lista_nodos = self.grafo[0]
        for i in range( len( lista_nodos ) ):
            for j in range ( len( lista_nodos ) - (i+1) ):
                nodoa = lista_nodos[i]
                nodob = lista_nodos[ (i+1) + j]
                print("Dist de "+nodoa+" a "+nodob+": " +str(np.linalg.norm(self.posiciones[nodob]-self.posiciones[nodoa])))

    def step(self):
        """
        step :: () -> ()
        Calcula un paso del algoritmo
        """
        #inicializa las fuerzas en (0,0)
        for nodo in self.grafo[0]:
            self.fuerzas[nodo] = np.array([0.0, 0.0])

        lista_nodos = self.grafo[0]     #Lista de nodos
        lista_aristas = self.grafo[1]   #Lista de aristas
        cantNodos = len( lista_nodos )  #Cantidad de nodos

        area = self.ancho * self.alto   #Area del canvas
        
        # computa fuerzas de atraccion (por aristas)
        for arista in lista_aristas:
            nodoa = arista[0]
            nodob = arista[1]
            fa = utils.calculo_atraccion(nodoa, nodob, self.posiciones, cantNodos, self.area, self.c2 )
            self.fuerzas[nodoa] += fa   # Sumamos la fuerza en un nodo y restamos en el otro, para simular fuerzas con sentido opuesto
            self.fuerzas[nodob] -= fa

        # computa repulsiones (por nodos)
        for i in range( len( lista_nodos ) ):
            for j in range ( len( lista_nodos ) - (i+1) ):
                nodoa = lista_nodos[i]
                nodob = lista_nodos[ (i+1) + j]
                fr = utils.calculo_repulsion(nodoa, nodob, self.posiciones, cantNodos, self.area, self.c1 )
                self.fuerzas[nodoa] -= fr
                self.fuerzas[nodob] += fr

        # calculamos la gravedad
        for nodo in lista_nodos:
            fg = utils.calculo_gravedad(self.posiciones[nodo], self.g )
            self.fuerzas[nodo] += fg
                

        # Limitamos la fuerza en base a la temperatura del sistema
        for nodo in lista_nodos:
            mod = np.linalg.norm(self.fuerzas[nodo])
            if mod > self.t:
                self.fuerzas[nodo] *= self.t/mod # Volvemos la fuerza un vector unitario y la multiplicamos por la temperatura

        # Aplicamos las fuerzas y limitamos las posiciones a estar contenidas en el canvas
        for nodo in lista_nodos:
            self.posiciones[nodo] += self.fuerzas[nodo]
            self.posiciones[nodo] = utils.corregir_posiciones(self.posiciones[nodo], self.ancho, self.alto)

        # Si se produce una colision (la distancia entre dos nodos es menor al epsilon del sistema)
        # aplicaremos una fuerza aleatoria entre una y dos veces la constante de repulsion
        # Realizaremos esto hasta que no se produzcan colisiones
        colision = True
        while colision: 
            colision = False
            for nodoa in lista_nodos:
                for nodob in lista_nodos:
                    delta = self.posiciones[nodob] - self.posiciones[nodoa]
                    dist = np.linalg.norm(delta)
                    if (dist < self.eps and nodoa != nodob):
                            fuerza = np.array([random.uniform(1, 2) * self.c2, random.uniform(1, 2) * self.c2])
                            self.posiciones[nodoa] += fuerza
                            self.posiciones[nodob] -= fuerza
                            self.posiciones[nodoa] = utils.corregir_posiciones(self.posiciones[nodoa], self.ancho, self.alto)
                            self.posiciones[nodob] = utils.corregir_posiciones(self.posiciones[nodob], self.ancho, self.alto)
                            colision =  True

    def layout(self):
        """
        layout :: () -> ()
        Funcion que realiza el algoritmo y muestra en pantalla
        """       
        
        plt.ion() # con esto permitimos actualizar el cuadro.

        # Loop principal de iteracion
        for iter in range(1, self.iters + 1):
            plt.clf()   # Limpia el plot
            self.step() # Realiza un calculo de un step del algoritmo
            
            self.t = self.ct * self.t   # Actualizamos la temperatura

            # Ploteamos si es la ultima iteracion o 
            # si refresh es distinto de cero ploteamos en las iteraciones multiplos de refresh
            if (iter == self.iters or (self.refresh and iter % self.refresh == 0)):
                utils.graficar(self, iter)
                # Tiempo entre cada frame, al ser tan pequeño es limitado por el tiempo de calculo de un step
                plt.pause(0.0000001)
            
            # Imprime las informacion por consola si se elegio el modo verbose
            if (self.verbose):
                print("Iteracion "+ str(iter))
                self.printPos()
                self.printFuer()
                self.printDist()

        # Muestra el ultimo plot y espera que se presione enter en la consola para terminar
        # plt.show()
        utils.graficar(self, iter)
        print("Pulse enter para continuar...")
        input()
    
        pass


def main():
    """
    Inicia el programa
    """
    # Definimos los argumentos de linea de comando que aceptamos
    # Los valores por defecto fueron calculados de forma empirica
    parser = argparse.ArgumentParser()

    # Verbosidad, opcional, False por defecto
    parser.add_argument(
        '-v', '--verbose',
        action='store_true',
        help='Muestra mas informacion al correr el programa'
    )
    # Cantidad de iteraciones, opcional, 50 por defecto
    parser.add_argument(
        '--iters',
        type=int,
        help='Cantidad de iteraciones a efectuar',
        default=100
    )
    # Cada cuantas iteraciones se replotea
    parser.add_argument(
        '--refresh',
        type=int,
        help='Cada cuantas iteraciones se refrescara la imagen',
        default=1
    )
    # Altura del canvas
    parser.add_argument(
        '-H','--height',
        type=int,
        help='Altura del canvas',
        default=800
    )
    # Anchura del canvas
    parser.add_argument(
        '-w','--width',
        type=int,
        help='Ancho del canvas',
        default=800
    )
    # Archivo del cual leer el grafo
    parser.add_argument(
        'file_name',
        help='Archivo del cual leer el grafo a dibujar'
    )
    # Constante de la fuerza de repulsion
    parser.add_argument(
        '-r','--repulsion',
        type=float,
        help='Constante de la fuerza de repulsion',
        default= 0.1
    )
    # Constante de la fuerza de atraccion
    parser.add_argument(
        '-a','--atraccion',
        type=float,
        help='Constante de la fuerza de atraccion',
        default= 10
    )
    # Temperatura inicial
    parser.add_argument(
        '-t','--temp',
        type=float,
        help='Temperatura incial del sistema',
        default= 150.0
    )
    # Constante de disminucion de temperatura
    parser.add_argument(
        '-T','--constTemp',
        type=float,
        help='Constante de disminucion de la temperatura',
        default= 0.99
    )
    # Constante de gravedad
    parser.add_argument(
        '-g','--gravity',
        type=float,
        help='Constante de la fuerza de gravedad',
        default= 0.06
    )
    # Distancia minima entre 2 nodos
    parser.add_argument(
        '-e','--eps',
        type=float,
        help='Distancia minima entre nodos',
        default= 0.001
    )
    # Mostrar nombres de los nodos
    parser.add_argument(
        '-n', '--names',
        action='store_true',
        help='Muestra los nombres de los nodos del grafo'
    )
    # Mostrar Ejes del grafo
    parser.add_argument(
        '--axes',
        action='store_true',
        help='Muestra los ejes donde se grafica el grafo'
    )
    # Tamaño de letra
    parser.add_argument(
        '--fontsize',
        type=float,
        help='Tamaño de letra',
        default=200.0
    )

    args = parser.parse_args()
    
    # Carga el grafo desde el archivo ingresado por consola
    grafo = utils.leer_grafo_archivo( args.file_name )

    layout_gr = LayoutGraph(
         grafo, 
         iters = args.iters,
         refresh = args.refresh, 
         ancho = args.height,
         alto = args.width,
         c1 = args.repulsion,
         c2 = args.atraccion,
         t0 = args.temp,
         ct = args.constTemp,
         g = args.gravity,
         eps = args.eps,
         verbose=args.verbose,
         nodeNames = args.names,
         fontsize = args.fontsize,
         axes = args.axes
     )

    layout_gr.layout()
    return


if __name__ == '__main__':
    main()
