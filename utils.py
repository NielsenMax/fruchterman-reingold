import random
import math
import numpy as np
import matplotlib.pyplot as plt

def randomrango(minimo, maximo):
    """
    randomrango :: (int, int) -> float
    Parametros:
    minimo: minimo del rango
    maximo: maximo del rango
    Calcula un numero aleatorio entre el minimo y el maximo
    """
    delta = maximo-minimo
    numero = minimo + random.uniform(0, 1) * delta
    return numero


def randomize_position (lista_nodos, ancho, alto):
    """
    randomrango :: (list(strings), int, int) -> dict(string: (float, float))
    Parametros:
    lista_nodos:    Lista de los nombres de los nodos
    ancho:          Ancho del canvas
    alto:           Alto del canvas
    Mapea en un dicionario, cada nombre con un punto aleatorio del canvas
    """
    posiciones = {}
    anchomin = -ancho/2
    anchomax = ancho/2
    altomin = -alto/2
    altomax = alto/2
    for nodo in lista_nodos:
        posiciones[nodo] = np.array([randomrango( anchomin , anchomax),randomrango( altomin , altomax)])
    return posiciones

def calculo_repulsion(nodoa, nodob, posiciones, cantNodos, area, C):
    """
    calculo_repulsion :: (string, string, dict(string: (float, float), int, int, float) -> (float, float))
    Parametros:
    nodoa:      Nombre del nodo A
    nodob:      Nombre del nodo B
    posiciones: Diccionario con las posiciones de los nodos
    cantNodos:  Cantidad de nodos
    area:       Area del canvas           
    C:          Constante de repulsion
    Calcula la fuerza de repulsion entre dos nodos
    """
    delta = posiciones[nodob] - posiciones[nodoa]
    dist = np.linalg.norm(delta)
    k = -math.sqrt( area / cantNodos ) * C
    fuerza = k*k / dist
    return delta*(fuerza/dist)

def calculo_atraccion(nodoa, nodob, posiciones, cantNodos, area, C):
    """
    calculo_atraccion :: (string, string, dict(string: (float, float), int, int, float) -> (float, float))
    Parametros:
    nodoa:      Nombre del nodo A
    nodob:      Nombre del nodo B
    posiciones: Diccionario con las posiciones de los nodos
    cantNodos:  Cantidad de nodos
    area:       Area del canvas           
    C:          Constante de atraccion
    Calcula la fuerza de atraccion entre dos nodos
    """
    delta = posiciones[nodob] - posiciones[nodoa]
    dist = np.linalg.norm(delta)
    k =  math.sqrt( area / cantNodos ) * C
    fuerza = dist*dist / k
    return delta*(fuerza/dist)

def calculo_gravedad(posicion, G):
    """
    calculo_gravedad :: ((float, float), float) -> (float, float)
    Parametros:
    posicion:   Vector con la posicion de un nodo
    G:          Constante gravitatoria
    Calcula la fuerza de gravedad ejercida sobre un nodo
    """
    dist = np.linalg.norm(posicion)
    return posicion* (-G/dist)

def corregir_posiciones(posicion, ancho, alto):
    """
    corregir_posiciones :: ((float, float), int, int) -> (float, float)
    Parametros:
    posicion:   Vector con la posicion de un nodo
    ancho:      Ancho del canvas
    alto:       Alto del canvas 
    Limita la posicion pasada a estar dentro del canvas 
    """
    if posicion[0] > ancho/2:
        posicion[0] = ancho/2
    if posicion[0] < -ancho/2:
        posicion[0] = -ancho/2
    if posicion[1] > alto/2:
        posicion[1] = alto/2
    if posicion[1] < -alto/2:
        posicion[1] = -alto/2
    return posicion
        

def leer_grafo_archivo(file_path):
    """
    leer_grafo_archivo :: (string) -> [list(string), list(list(string))]
    Parametros:
    file_path:  Path al archivo a leer
    Lee un grafo de un archivo y lo convierte a un grafo en formato lista
    """
    count = 0

    grafo_lista = []
    lista_nodos = []
    lista_aristas = []

    with open(file_path, 'r') as f:
        lineas = f.readlines()
        numNodos = int( lineas.pop(0) )
        for linea in lineas:
            if count < numNodos:
                lista_nodos.append( linea.rstrip('\n') )
            else:
                arista = linea.split(' ')
                lista_aristas.append( (arista[0], arista[1].rstrip('\n') ) )
            count = count + 1

    grafo_lista = [ lista_nodos, lista_aristas ]
    return grafo_lista

def graficar (grafo, iter):
    """
    graficar :: (LayoutGraph, int) -> ()
    Parametros:
    grafo:  Grafo
    iter:   Numero de la iteracion actual
    Grafica un grafo en pantalla 
    """
    nodos = len( grafo.grafo[0] )
    lista_aristas = grafo.grafo[1]
    data = []
    # Ploteamos los nodos
    for nodo in grafo.grafo[0]:
        x = grafo.posiciones[nodo][0]
        y = grafo.posiciones[nodo][1]
        if grafo.nodeNames:
            plt.scatter(x, y, grafo.fontsize, '#FF0000', '$'+ nodo +'$') # Nodos con nombres
        else:
            plt.scatter(x, y, grafo.fontsize, facecolors='#FF0000')      # Nodos anonimos

    # Cargamos las aristas en el formato que requiere plot
    for arista in lista_aristas:
        nodoa = arista[0]
        nodob = arista[1]
        xx1 = grafo.posiciones[nodoa][0]
        yy1 = grafo.posiciones[nodoa][1]
        xx2 = grafo.posiciones[nodob][0]
        yy2 = grafo.posiciones[nodob][1]
        data.append(  (xx1,xx2)  )
        data.append(  (yy1,yy2)  )
        data.append(  'black' )
    
    # Ploteamos las aristas
    plt.plot(*data)
    # Agregamos un titulo
    plt.title("Temp= " + str( round(grafo.t) ) + " iter = " + str(iter) )
    # Fijamos el canvas, agregando un padding del fontsize/50 (fue calculado empiricamente con un ojimetro)
    plt.xlim(-(grafo.ancho/2 + grafo.fontsize / 50),(grafo.ancho/2 + grafo.fontsize / 50))
    plt.ylim(-(grafo.alto/2 + grafo.fontsize / 50),(grafo.alto/2 + grafo.fontsize / 50))
    # Elimina los ejes
    if (not grafo.axes) :
        plt.axis("off") 
    # Muestra el plot en pantalla
    plt.show()
